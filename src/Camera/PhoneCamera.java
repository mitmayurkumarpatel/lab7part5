package Camera;

/**
 *
 * @author mitpatel
 */
class Phonecamera extends DigitalCamera
{
     int ExternalMemorySize;
    
     public Phonecamera(String Make, String Model, double MegaPixels,int ExternalMemorySize ) 
    {
       super(Make,Model,MegaPixels);
       this.ExternalMemorySize = ExternalMemorySize;
    }
     
    public int getExternalMemorySize() 
    {
        return this.ExternalMemorySize;
    }
    
   String describeCamera()
    {
        return "Make is : " +super.getMake() + "\nModel is : " +super.getModel() + "\nMegapixels is : " +super.getMegaPixels()+ "\nInternal Memory size is :  " +this.ExternalMemorySize+"GB";
    }

   
            
}
