package Camera;

/**
 *
 * @author mitpatel
 */
class PointAndShootCamera extends DigitalCamera
{
     int InternalMemorySize;
    
     public PointAndShootCamera(String Make, String Model, double MegaPixels,int InternalMemorySize ) 
    {
       super(Make,Model,MegaPixels);
       this.InternalMemorySize = InternalMemorySize;
    }
     
    public int getInternalMemorySize() 
    {
        return InternalMemorySize;
    }
   String describeCamera()
    {
        return "Make is : " +super.getMake() + "\nModel is : " +super.getModel() + "\nMegapixels is : " +super.getMegaPixels()+ "\nInternal Memory size is :  " +this.InternalMemorySize+"GB";
        
    }

   
            
}
