package Camera;
/**
 *
 * @author mitpatel
 */
public abstract class DigitalCamera 
{
     String Make;
     String Model;
     double MegaPixels;

     public DigitalCamera(String Make, String Model, double MegaPixels) {
        this.Make = Make;
        this.Model = Model;
        this.MegaPixels = MegaPixels;
    }
     
    public String getMake() {
        return Make;
    }

    public String getModel() {
        return Model;
    }

    public double getMegaPixels() {
        return MegaPixels;
    }

   
    
    //abstract method
      abstract String describeCamera();
    
    
}
